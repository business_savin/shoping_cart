$(function () {

    CART_init = function (){

        if($.cookie('cart_nwblnc')==undefined){
            CART_save(0);
            CART_information_count();
            return 0;
        }else{
            CART_information_count();
            return $.cookie('cart_nwblnc');
        }
    }
 
    CART_get = function (){
        return JSON.parse($.cookie('cart_nwblnc'));
    }
    
    CART_information_count = function (){
        var get = CART_get();
        var count = 0;
        if(get!==0){//isEmptyObject
            $.each(get, function(index, value) {
                count = count + parseInt(get[index]['quantity'],10);
            })
            $('.basket-top-i-basket').html(count);
        }else{
            $('.basket-top-i-basket').html(count);
        }
    }
    
    CART_info_basket = function (element){

        var get = CART_get();
        //пустой изначально
        if(get===0){
            $(element).html('<h3>Ваша корзина пуста!</h3>');
            return true;
        }
        
        if(get!==0){
            var str = '';
            var total_all = 0;
            var tmp_prc = 0;
            var sizes = '';
            var slctd = '';
                
                str +='<div class="basket_row"><table>';
                str +='<tr class="tr_line">';
                str +='<td class="_200"><h4>Товар</h4></td>'; 
                str +='<td class="_300"></td>';   
                str +='<td class="_100"><h4>Количество</h4></td>';
                str +='<td class="_50"></td>';
                str +='<td class="_100"><h4>Стоимость</h4></td>'; 
                str +='<td></td>';
                str +='</tr>';
            $.each(get, function(index, value) {
                sizes = '';
                tmp_prc = value['price'].replace(/\s+/g, '');
                total_item = (parseInt(tmp_prc,10) * value['quantity']);
                total_all += total_item;
                str +='<tr class="tr_line">';
                str +='<td><img src="'+value['src']+'" alt="" /></td>';
                
                
                str +='<td><h3>'+value['title']+'</h3>';
                
                    str +='<table><tr><td><strong class="price-cart">'+value['price']+'&nbsp;Р</strong></td>';
                    $.each(value['sizes'], function(indx, val) {
                        if(value['size_select']===val){slctd='selected'}else{slctd=''}
                        sizes += '<option value="'+val+'" '+slctd+'>'+val+'</option>';
                        console.log(indx, val);
                    })
                    str +='<td><div class="basc-item-size">Размер:<div></td><td><select class="basc-item-size-select form-control" data-cart-item-id="'+value['id']+'">'+sizes+'</select></td></tr></table>';
                
                str +='</td>';
                
                
                str +='<td><table><tr><td><strong class="cart-min" data-cart-item-id="'+value['id']+'">-</strong></td><td><input type="text" value="'+value['quantity']+'" class="form-control count"  data-cart-item-id="'+value['id']+'"></td><td><strong class="cart-plus" data-cart-item-id="'+value['id']+'">+</strong></td></tr></tbody></table></td>';
                str +='<td>&nbsp;X</td>';
                str +='<td><div class="cart-item-total-price">'+total_item+'&nbsp;Р</div></td>';
                str +='<td><div class="cart_del_item" data-cart-id="'+value['id']+'" >[X]</div></td>';  
                str +='</tr>';
            });
                str +='<tr>';
                str +='<td></td>';
                str +='<td></td>';
                str +='<td></td>';
                str +='<td><h4>ИТОГО</h4></td>'; 
                str +='<td><div class="cart-total-price">'+total_all+'&nbsp;Р</div></td>';
                str +='<td></td>';  
                str +='</tr>';
            
            str +='</table></div>';
            str += CART_form();
            
            $(element).html(str);
            $('#cart-Phone').mask("+7 (999) 999-99-99");
            return true;
        }
    }
                   
    CART_set = function (data,data2){ 
        
        var get = CART_get();
        
        if(get===0){
            $.cookie('cart_nwblnc', JSON.stringify(data), {expires: 30, path:'/'});
            CART_information_count();
            return true;
        }
    
        var access = 0;
        var inds = 0; 
        $.each(get, function(index, value) { 
            inds = index;
            if(data[0]['id']==value['id']){
                get[index]['quantity'] = get[index]['quantity'] + parseInt(data[0]['quantity'],10);
                access = 0;
                CART_save(get);
                CART_information_count();
                return false;
            }else{
                access = 1;
            }
        });
        
        if(access==1){
            get[inds+1]= data[0];
        }
                 
        CART_save(get);
        CART_information_count();
    }
    
    CART_save = function (data){
        if($.isEmptyObject(data)){
            data = 0;
        }
        var inf = JSON.stringify(data);
        $.cookie('cart_nwblnc', inf, {expires: 30, path:'/'});
    }
    
    CART_delete = function (elem){
        var id_del = $(elem).data('cart-id');
        var get = CART_get();
        var new_get = {};
        var new_index = 0;
        $.each(get, function(index, value) {
            if(id_del!=value['id']){
                new_get[new_index] = get[index];
                new_index++;
            }
        });
        
        CART_save(new_get);
        CART_information_count();
        CART_info_basket('.basket-content');
    }
    
    CART_adToCartAnimation = function (elem){
        
        var item = $(elem)
        item.clone()  
            .css({'position' : 'absolute', 'z-index' : '100'})  
            .appendTo(item.parent())  
            .animate({opacity: 0.5,   
                            marginTop: -700, /* Важно помнить, что названия СSS-свойств пишущихся  
                            через дефис заменяются на аналогичные в стиле "camelCase" */  
                            width: 50,   
                            height: 50}, 700, function() {  
                    $(this).remove();  
              });
    }
    
    CART_form = function (){
        
        return '<form role="form" id="cart-form" class="q-cart-form form-horizontal">'+
                '<div class="form-group">'+
                    '<label for="cart-Name" class="col-sm-3 control-label">Ваше имя</label>'+
                    '<div class="col-sm-9"><input type="text" class="form-control" id="cart-Name" name="name" placeholder="Ваше имя" value=""></div>'+    
                '</div><div class="form-group">'+
                    '<label for="cart-Phone" class="col-sm-3 control-label">Телефон</label>'+
                    '<div class="col-sm-9"><input type="text" class="form-control" id="cart-Phone" name="phone" placeholder="Телефон" value=""></div>'+
                '</div><div class="form-group">'+
                    '<label for="cart-Email" class="col-sm-3 control-label">E-mail</label>'+
                    '<div class="col-sm-9"><input type="text" class="form-control" id="cart-Email" name="email" placeholder="E-mail" value=""></div>'+
                '</div><div class="form-group">'+            
                    '<label for="cart-Adress" class="col-sm-3 control-label">Адрес доставки</label>'+
                    '<div class="col-sm-9"><input type="text" class="form-control" id="cart-Adress" name="addres" placeholder="Адрес доставки" value=""></div></div><button type="submit" class="btn btn-warning q-cart-form-but-sumbit">Оформить заказ</button></div></form>';
        
    }
    
    CART_ajax = function(){

        var get = CART_get();
        var dat = {}
        var access = true;
        $(".q-cart-form input[type='text']").each(function(){
            if('phone'===$(this).attr('name')){
                if($(this).val().length<=5){
                    $(this).css('border-color','#f00');
                    access = false;
                    return false; 
                }
            }
            dat[$(this).attr('name')]=$(this).val()
        });
        if(access){ 
            $.each(get, function(index, value) {
                delete get[index]['src'];
                delete get[index]['sizes'];
            }) 
            dat['get'] = get;
            $.ajax({
                    type: 'POST',
                    url: 'index.php?route=/common/home/cartorder',
                    data: dat,
                    dataType: 'json',
                    success: function(data){
                        console.log(data)
                        window.location.href = "http://newbalance.net.ru/tnxbasket";
                        $('#myModal').modal('hide');
                        CART_save(0);
                        CART_information_count();
                    },
                    error: function(xhr, ajaxOptions, thrownError){
                        window.location.href = "http://newbalance.net.ru/tnxbasket";
                        CART_save(0);
                        CART_information_count();
                    }
            });
            
        }/*access*/
        return false;
    }
    
    /* Action */
    
    CART_init();
    
    /* DOM manipulation */
    
    $('.item-ico-basket, .big-item-ico-basket').on('click', function(){
        
        CART_adToCartAnimation(this);
        var item = $(this).parent();
        var id = item.data('i-id');
        var src = item.data('i-sr'); 
            src = src.replace("350x300",'200x150');
        var title = item.data('i-ti');
        var price = item.data('i-pr');
        var orice_old = item.data('i-po');
 
        
        if( item.hasClass('big-item-add-to-cart') ){
            var sizes = item.parent().find('.big-size-invis').html();  
        }else{
            var sizes = item.parent().find('.item-size').html(); 
        }
            sizes = sizes.replace(/<i>/g,"").replace(/<\/i>/g,",");
            sizes = $.trim(sizes);
            sizes = sizes.substring(0, sizes.length - 1);
            sizes = sizes.split(',');
        
        var size_select = sizes[0];
        
        var data = [{
                     "id": id,
                     "src": src,
                     "title": title,
                     "price": price,
                     "orice_old": orice_old,
                     "quantity": 1,
                     "sizes": sizes,
                     "size_select":size_select
                    }];
        CART_set(data);
    });
    
    $('.basket-top').on('click', function(){
        $('#window-basket').modal();
        CART_info_basket('.basket-content');
    });
        
    $(".basket-top")
        .on("mouseenter", function () {
            $('.basket-top-hover').fadeIn(200);
            $(this).append("<div class='basket-top-ofomit-zakaz'>Оформить заказ</div>")
            $('.basket-top').height(65);
        })
        .on("mouseleave", function () {
            $('.basket-top-hover').fadeOut(200);
            $(this).find('.basket-top-ofomit-zakaz').remove()
            $('.basket-top').height(30);
        });
    
    $('.header-search-form').after("<div class='basket-top-hover'></div>");
    
    $(document).on('submit', 'form#cart-form', function(){
        event.preventDefault();
        CART_ajax();        
    })
    
    /* ############################################ */
    
    $(document).on('click','.cart_del_item', function(){ 
        CART_delete(this);
    });
    
    $(document).on('change','.basc-item-size-select', function(){
        var get = CART_get();
        var id = $(this).data('cart-item-id');
        var sizsel = $("option:selected", this).val();
        $.each(get, function(index, value) {
            if(value['id']==id){ 
                get[index]['size_select'] = sizsel;
                CART_save(get);
                CART_information_count();
                return true;
            }
        })
    });
    
    $(document).on('click','.cart-min,.cart-plus', function(){
        
        var get = CART_get();
        var cnt = $(this).parent().parent().find('.count');
        var ttl = 1;
        var id = $(this).data('cart-item-id');

        if($(this).hasClass('cart-min')){
            ttl = parseInt(cnt.val(),10)-1;
            $.each(get, function(index, value) {
                if(value['id']==id){
                    if(ttl<=1){ttl=1}
                    get[index]['quantity'] = ttl;
                    cnt.val(ttl);
                    CART_save(get);
                    CART_information_count();
                    return true;
                }
            })
        }else{
            ttl = parseInt(cnt.val(),10)+1;
            $.each(get, function(index, value) {
                if(value['id']==id){
                    get[index]['quantity'] = ttl;
                    cnt.val(ttl);
                    CART_save(get);
                    CART_information_count();
                    return true;
                }
            })
        }
        CART_info_basket('.basket-content');
    })
 
    $(document).on('blur', '.count', function(){
        
        var get = CART_get();
        var id = $(this).data('cart-item-id');
        var ttl = parseInt($(this).val(),10);
        
        $.each(get, function(index, value) {
                if(value['id']==id){
                    if(ttl<=1){ttl=1}
                    get[index]['quantity'] = ttl;
                    CART_save(get);
                    CART_information_count();
                    CART_info_basket('.basket-content');
                    return true;
                }
        });
        
    });
})